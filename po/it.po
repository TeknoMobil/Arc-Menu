# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Arc Menu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Arc Menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-19 15:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: prefs.js:42
msgid "General"
msgstr ""

#: prefs.js:54
msgid "Behaviour"
msgstr "Comportamento"

#: prefs.js:63
msgid "Disable activities hot corner"
msgstr "Disabilita hot corner"

#: prefs.js:85
msgid "Set menu hotkey"
msgstr "Imposta scorciatoia da tastiera"

#: prefs.js:91
msgid "Undefined"
msgstr "Non definito"

#: prefs.js:92
msgid "Left Super Key"
msgstr "Tasto Super sinistro"

#: prefs.js:93
msgid "Right Super Key"
msgstr "Tasto Super destro"

#: prefs.js:105
msgid "Enable custom menu keybinding"
msgstr "Abilita scorciatoia personalizzata"

#: prefs.js:112
msgid "Syntax: <Shift>, <Ctrl>, <Alt>, <Super>"
msgstr "Sintassi: <Shift>, <Ctrl>, <Alt>, <Super>"

#: prefs.js:153
msgid "Button appearance"
msgstr "Aspetto pulsante"

#: prefs.js:165
msgid "Text for the menu button"
msgstr "Testo del pulsante"

#: prefs.js:171
msgid "System text"
msgstr "Testo di sistema"

#: prefs.js:174
msgid "Custom text"
msgstr "Testo personalizzato"

#: prefs.js:198
msgid "Set custom text for the menu button"
msgstr "Modifica il testo del pulsante"

#: prefs.js:218
msgid "Enable the arrow icon beside the button text"
msgstr "Abilita icona affiancata al testo del pulsante"

#: prefs.js:240
msgid "Select icon for the menu button"
msgstr "Scegli un'icona per il pulsante"

#: prefs.js:250
msgid "Please select an image icon"
msgstr "Scegli un'immagine"

#: prefs.js:255
msgid "Arc Menu Icon"
msgstr "Icona Arc Menu"

#: prefs.js:256
msgid "System Icon"
msgstr "Icona di sistema"

#: prefs.js:257
msgid "Custom Icon"
msgstr "Icona personalizzata"

#: prefs.js:283
msgid "Icon size"
msgstr "Dimensione icona"

#: prefs.js:283
msgid "default is"
msgstr "la dimensione di default è"

#: prefs.js:323
msgid "Appearance"
msgstr "Aspetto"

#: prefs.js:332
msgid "Customize menu button appearance"
msgstr "Personalizza pulsante"

#: prefs.js:342
msgid "Icon"
msgstr "Icona"

#: prefs.js:343
msgid "Text"
msgstr "Testo"

#: prefs.js:344
msgid "Icon and Text"
msgstr "Icona e testo"

#: prefs.js:345
msgid "Text and Icon"
msgstr "Testo e icona"

#: prefs.js:370
msgid "Menu position in panel"
msgstr "Posizione nel pannello"

#: prefs.js:377
msgid "Left"
msgstr "Sinistra"

#: prefs.js:380
msgid "Center"
msgstr "Centro"

#: prefs.js:384
msgid "Right"
msgstr "Destra"

#: prefs.js:461
msgid "About"
msgstr "Informazioni"

#: prefs.js:490
msgid "Arc-Menu"
msgstr "Arc-Menu"

#: prefs.js:495
msgid "version: "
msgstr "versione: "

#: prefs.js:503
msgid "GitLab Page"
msgstr ""

#: menu.js:272
msgid "Home"
msgstr "Home"

#: menu.js:456 menu.js:461
msgid "Software"
msgstr "Software"

#: menu.js:466
msgid "Settings"
msgstr "Impostazioni"

#: menu.js:471
msgid "Tweak Tool"
msgstr "Strumento di personalizzazione"

#: menu.js:476
#, fuzzy
msgid "Tweaks"
msgstr "Strumento di personalizzazione"

#: menu.js:481
msgid "Terminal"
msgstr ""

#: menuWidgets.js:86
msgid "Activities Overview"
msgstr "Panoramica Attività"

#: menuWidgets.js:199
msgid "Power Off"
msgstr "Spegni"

#: menuWidgets.js:212
msgid "Log Out"
msgstr "Termina sessione"

#: menuWidgets.js:225
msgid "Suspend"
msgstr "Sospendi"

#: menuWidgets.js:243
msgid "Lock"
msgstr "Blocca schermo"

#: menuWidgets.js:265
msgid "Back"
msgstr "Indietro"

#: menuWidgets.js:483
msgid "Favorites"
msgstr "Preferiti"

#: menuWidgets.js:616
msgid "Type to search…"
msgstr "Digita per cercare…"

#: menuWidgets.js:781
msgid "Applications"
msgstr "Applicazioni"

#~ msgid "Webpage"
#~ msgstr "Pagina web"
